var add;
add = function (x, y) {
    return x + y;
};
// Asserting types
var getChangeOfLife = function (person) {
    return person.sex == "male" ? { remained: 68 - person.age } : { remained: 73 - person.age };
};
getChangeOfLife({ age: 38, sex: "male", country: "Australia" });
/*
interface Printable {
    colors?: string[];
    readonly paperSize: string;
    print(): void;
}

let printIt = function(what: Printable){
    what.print();
}

let printableObject: Printable = {
    paperSize : "A3",
    print: ()=> console.log("printableobject is printed")
};

printIt(printableObject);

class PdfDoc implements Printable{
    public paperSize: string = "A4";
    public print():void{
        console.log("pdf doc is printed");
    }
}
*/ 
