var Animal = /** @class */ (function () {
    //public weight: number = 0;
    function Animal(species, weight) {
        this.weight = weight;
        this.species = "";
        this._numberOfKidneys = 2;
        this.species = species;
        this.species = species;
    }
    return Animal;
}());
var koko = new Animal("Gorilla", 200);
koko.weight = 250;
// Static members
var MyMathHeper = /** @class */ (function () {
    function MyMathHeper() {
    }
    MyMathHeper.Add = function (x, y) {
        return x = y;
    };
    MyMathHeper.pi = 3.14159;
    return MyMathHeper;
}());
// koko.species = "Chimpanzee" // can not change if we call readonly
// var howManyKidneys = koko.numberOfKidneys;
// koko.numberOfKidneys = 3;
