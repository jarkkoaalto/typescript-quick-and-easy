class Person {
    public name : string;
    protected money: number = 10;

    constructor (name) {
        this.name = name;
    }
}

class Employee extends Person{

    public salary : number = 0;

    constructor (name, salary){
        super(name);
        this.salary = salary;
    }
/*
    public checkAccess(){
        this.name = "";
        this.money = 100;
    }
*/
}

let emp1  = new Employee("billy jean", 100000);
emp1.name = "James Bill";
console.log(`${emp1.name} ${emp1.salary}`); // result: "James Bill 100000"
