class Animal {
    readonly species: string = "";
    private _numberOfKidneys = 2;

    //public weight: number = 0;

    constructor(species, public weight){
        this.species = species;
        this.species = species;
    }

    /*
    public setNumberOfKidneys(value){
        this._numberOfKidneys = value;
    }

    get numberOfKidneys(){
        return this._numberOfKidneys;
    }
    set numberOfKidneys(value){
        if(value < 0) return;
        this._numberOfKidneys = value;
    }
    */
}

let koko = new Animal("Gorilla", 200);
koko.weight = 250;

// Static members
class MyMathHeper{
    public static pi: number = 3.14159;
    public static Add(x: number, y: number): number{
        return x = y;
    }
}

let myBankPassword = MyMathHeper.Add(1,MyMathHeper.pi);

// koko.species = "Chimpanzee" // can not change if we call readonly
// var howManyKidneys = koko.numberOfKidneys;
// koko.numberOfKidneys = 3;