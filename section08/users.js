var user = /** @class */ (function () {
    function user(name, monthlySalary) {
        this.name = name;
        this.monthlySalary = monthlySalary;
        this.name = name;
        this.monthlySalary = monthlySalary;
    }
    return user;
}());
var users = [
    new user("Ms. Dolphin", 8500),
    new user("Mr. Cat", 7500),
];
var userHelper = /** @class */ (function () {
    function userHelper() {
    }
    userHelper.getAll = function () {
        return users.slice(); // a copy of original data
    };
    return userHelper;
}());
