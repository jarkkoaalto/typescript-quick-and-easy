System.register([], function (exports_1, context_1) {
    "use strict";
    var productRreporter, productEmailSender;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [],
        execute: function () {
            productRreporter = /** @class */ (function () {
                function productRreporter() {
                }
                productRreporter.remainedProductNumber = function () {
                    return 10;
                };
                return productRreporter;
            }());
            exports_1("productRreporter", productRreporter);
            productEmailSender = /** @class */ (function () {
                function productEmailSender() {
                }
                productEmailSender.emailTheBigBoss = function () {
                    console.log("Report is emailed to big boss");
                };
                return productEmailSender;
            }());
            exports_1("productEmailSender", productEmailSender);
        }
    };
});
