"use strict";
exports.__esModule = true;
var operators = /** @class */ (function () {
    function operators() {
    }
    operators.times = function (x, y) {
        return x * y;
    };
    return operators;
}());
exports.mathOperators = operators;
var functions = /** @class */ (function () {
    function functions() {
    }
    functions.power = function (x, y) {
        return Math.pow(x, y);
    };
    return functions;
}());
exports.mathFunctions = functions;
exports.pi = Math.PI;
exports["default"] = {
    pi: exports.pi,
    oprators: operators,
    functions: functions
};
