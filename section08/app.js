System.register(["./product"], function (exports_1, context_1) {
    "use strict";
    var product_1, testThings;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [
            function (product_1_1) {
                product_1 = product_1_1;
            }
        ],
        execute: function () {
            //import * as userManager from "./users";
            testThings = function () {
                console.log("Number of products: " + product_1.productRreporter.remainedProductNumber());
                product_1.productEmailSender.emailTheBigBoss();
                var users = userManager.userHelper.getAll();
                for (var _i = 0, users_1 = users; _i < users_1.length; _i++) {
                    var user_1 = users_1[_i];
                    console.log(user_1.name);
                }
            };
            document.getElementById("testBtn").addEventListener("click", testThings);
        }
    };
});
