class Entity {
    constructor (public id: number) {}
}

class User extends Entity {
    constructor(id: number, public name: string) {super(id);}
}

class Product extends Entity {
    constructor (id: number, public price: number){super(id);}
}

function EntitySerializer<T extends Entity>(input:T): string{
    return JSON.stringify(input);
}


/* One solution
function UserSerializer(input: User): string{
    return JSON.stringify(input);
}

function ProductSerializer(input: Product): string{
    return JSON.stringify(input);
}
*/

let user = new User(1,"Epic User");
let product = new Product(2,155);

let serializedUser = EntitySerializer<User>(user);
let serializedUserProduct = EntitySerializer<Product>(product);

// Not valid
// let wrong = EntitySerializer<number>(2);

