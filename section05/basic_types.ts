// number
let n : number = 14;
n = 0x11; // hex
n = 0b1001; // binary
n = 0o21; // octal

//boolean
let b : boolean = true;
b = false;
b =!!(n); ///n == 0 ? false : true
b = !!(null); // false 


//string
let s: string ="Hello buddy";
s = "Hello buddy";
s = `My number is ${n}`; // My number is 14

// undifined and null
n = undefined; 
s = undefined;
b = undefined;
n = null;
s = null;
b = null;

//any
let a: any;
a = 2;
a = `string`;
a = false;

// void
let v: void;
v = undefined;
v = null;


// array
let nArray: number[] = [1,2,3];
let sArray: Array<string> = ["Hi","hello","hey"];
let aArray: any[] = [1,"1"];

// tuple
let t: [string, number, number] = ["Joe Down", 56, 402020];
t[1] = 53;

// enum
enum Color {red, Green, Blue};
let e: Color = Color.Green;
e = 0;
n = Color.red;

// object
let o: object = {
    name: "Doe"
}

let o2: {name: string} = {
    name: "Doe"
};