// number
var n = 14;
n = 0x11; // hex
n = 9; // binary
n = 17; // octal
//boolean
var b = true;
b = false;
b = !!(n); ///n == 0 ? false : true
b = !!(null); // false 
//string
var s = "Hello buddy";
s = "Hello buddy";
s = "My number is " + n; // My number is 14
// undifined and null
n = undefined;
s = undefined;
b = undefined;
n = null;
s = null;
b = null;
//any
var a;
a = 2;
a = "string";
a = false;
// void
var v;
v = undefined;
v = null;
// array
var nArray = [1, 2, 3];
var sArray = ["Hi", "hello", "hey"];
var aArray = [1, "1"];
// tuple
var t = ["Joe Down", 56, 402020];
t[1] = 53;
// enum
var Color;
(function (Color) {
    Color[Color["red"] = 0] = "red";
    Color[Color["Green"] = 1] = "Green";
    Color[Color["Blue"] = 2] = "Blue";
})(Color || (Color = {}));
;
var e = Color.Green;
e = 0;
n = Color.red;
// object
var o = {
    name: "Doe"
};
var o2 = {
    name: "Doe"
};
