# TypeScript Quick and Easy #

This is practical course to learn TypeScript rapidly and deeply.

## About this course ##

Course goals
- rapidly up andrun with typeScript
- Write your Javascript application in TypeScript
- Essential features but not a reference book

Who is this course for?
- who knows javascript
- and basic of programming

- Section 1: About this course
- Section 2: Introduction
- Section 3: Setting up our development environment
- Section 4: ES6 Features
- Section 5: Types
- Section 6: Classes and Interfaces
- Section 7: Generics
- Section 8: Modules
- Section 9: Decorators
- Section 10: Real world tips

TypeScript
- programming language
- Strict superset of Javascript
- Comes with a transpiler to convert the code to Javascript
- Optional static type
- Optional class-based object-oriented programming
- development and maintained Microsoft

installing typescript:
https://www.typescriptlang.org or using npm install -g typescript

typeScript compailing
- one file: tsc testscript.ts
- directory : tsc -p . - if you are added tscript.json - file

Check typescript changes, you can use command
```
tsc -w
```
You can stop watch mode using (ctrl + C) command


### Modules ###
- A feature of ES6 version of JavaScript
- Changes the architecture of your application


Traditional Javascript programming
```
users.js -> Available to all in global scope
```
Modern Javascript programming
``` 
users.js, app.js, math.js are visible outside
```

### Installing a simple web-server ###

Installation via nmp:
```
npm install http-server -g
```
This will install http-server globally so that it may be run from the command line

usage:
```
http-server [path][options]
```

### Decorators ###
- Modify behavior of classes, methods, accessors, parameters and properties
- Proposal for JavaScript
- Experimental feature of TypeScript
- Framework designer's biskuit

## Real wordl tips

- Debugging
- Using JS libraries

### using js libraries ###
```
npm install --save @types/jquery
```
