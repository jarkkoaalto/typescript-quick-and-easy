var test_string = function() {
var person = {
    name: "Buddy Holy",
    employed: false
};
var olfFashionadString = "Name" + person.name + "Employed: " + (person.employed ? "yes":"no") + "\n";

console.log("old ways result");
console.log(olfFashionadString);

var niceString = `Name ${person.name} Employed: ${person.employed ? "yes":"no"}`;
console.log("new way result");
console.log(olfFashionadString);
};